# VisualHull

Uni Project for constructing a visual hull from silhouettes
See the webpages for more detailed information and documentation on [VisualHull](https://mirorasu.gitlab.io/visualhull/)

## Description
This is my university project at _Universität Heidelberg_ for the subject _Informatik_, as part of the module _Anfängerpraktikum_ in the Summer Semester 2021.
The goal is a program which takes in some input parameters and silhouettes of a 3D object taken from different perspectives and positions, then it will (re)construct the 3D model, so that the projected silhouettes of this model align with the input files.
There are different ways of this construction. In this project I chose the approach to carve a big cube consisting of voxels, such that the remaining voxels project a silhouette/view contained in the input silhouettes.

## Installation
The scripts will work on any system that supports Python 3 (tested on Python 3.9)
Also following modules have to be preinstalled (e.g. via pip install):
- NumPy ([install instructions](https://numpy.org/install/))
- PyPNG ([install instructions](https://github.com/drj11/pypng#installation))
- Only for visualization and view code: pygame ([install instructions](https://www.pygame.org/wiki/GettingStarted))

This one-liner to install the prerequisites might work for you:
```
pip install numpy pypng pygame
```

## Usage
Usage is described on the documentation pages: [Main Parameters](https://mirorasu.gitlab.io/visualhull/docs/parameters/).

## Support
For any questions, contact me here via GitLab.

## License
As you can see, this project is open source and can be used freely. If you happen to use it for your own projects, a small credit would be much appreciated. ^^
Legally the project is under the [MIT License](LICENSE)
Please keep in mind that some model data (i.e. [examples/minerva/source_model](examples/minerva/source_model)) are not mine but underlie the CC-BY license. See the License there for more details.

## Project status
The project is in a final state for now. It might be updated in the future.

