# Minerva Example

# Silhouette Pictures
For this example, the silhouette pictures `silhouettes/*.png` were taken in Blender in the project file `silhouettes/photo_studio.blend`.

# Source Model
The original source model `source_model/Minerva.obj` of the Roman goddess was created by "Macías" and is licensed under CC-BY. Click [here](source_model/LICENSE) for more details on the license.
