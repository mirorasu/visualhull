from numpy import array, ones, zeros, matmul
import transformations as T
import png

class PixelGrid:
    def __init__(self, pixels_x, pixels_y, pixel_size=1, position=(0,0,0), rotation=(0,0,0), init=True):
        self._pixels_x = int(pixels_x)
        self._pixels_y = int(pixels_y)

        self._pixel_size = pixel_size
        self._position = position
        self._rotation = rotation
        self._view_matrix = T.view_matrix(self.position, self.rotation)

        if init:
            self._grid = ones((self.pixels_x, self.pixels_y), dtype=bool)
        else:
            self._grid = zeros((self.pixels_x, self.pixels_y), dtype=bool)

    @classmethod
    def by_size(cls, width, height, pixel_size=1, position=(0,0,0), rotation=(0,0,0), init=True):
        return cls(width//pixel_size, height//pixel_size, pixel_size, position, rotation, init)
    
    @classmethod
    def by_png(cls, filename, pixel_size=1, position=(0,0,0), rotation=(0,0,0), dark_spot=(0,0,0,255), light_spot=None):
        picture = png.Reader(filename).asRGBA()
        width, height = picture[0], picture[1]
        pixel_grid = cls(width, height, pixel_size, position, rotation)

        rows = list(picture[2])
        if light_spot is None:
            for col in range(width):
                for row in range(height):
                    pixel_grid[col][row] = tuple(rows[row][col*4:(col+1)*4]) == dark_spot
        else:
            for col in range(width):
                for row in range(height):
                    pixel_grid[col][row] = tuple(rows[row][col*4:(col+1)*4]) != light_spot
        
        return pixel_grid

    def __getitem__(self, key):
        return self._grid[key]

    @property
    def pixels_x(self):
        return self._pixels_x

    @property
    def pixels_y(self):
        return self._pixels_y
    
    @property
    def pixel_size(self):
        return self._pixel_size
    
    @property
    def position(self):
        return self._position
    
    @position.setter
    def position(self, position=(0,0,0)):
        self._position = position
        self._view_matrix = T.view_matrix(self._position, self._rotation)

    @property
    def rotation(self):
        return self._rotation
    
    @rotation.setter
    def rotation(self, rotation):
        self._rotation = rotation
        self._view_matrix = T.view_matrix(self._position, self._rotation)

    @property
    def view_matrix(self):
        return self._view_matrix

    @property
    def width(self):
        return self.pixels_x * self.pixel_size

    @property
    def height(self):
        return self.pixels_y * self.pixel_size
    
    def pixel_vertices(self, pixel_index=(0,0)):
        return [(    pixel_index[0],     pixel_index[1]),
                (1 + pixel_index[0],     pixel_index[1]),
                (1 + pixel_index[0], 1 + pixel_index[1]),
                (    pixel_index[0], 1 + pixel_index[1])]

class ViewPlane(PixelGrid):
    def __init__(self, pixels_x, pixels_y, pixel_size=1, position=(0,0,0), rotation=(0,0,0), init=True, perspective=False, fov=90, near=0.1, far=100):
        super().__init__(pixels_x, pixels_y, pixel_size, position, rotation, init)
        self._perspective = perspective
        self._near = near
        self._far = far

        if self.perspective:
            aspect = self.pixels_x / self.pixels_y
            self._fov = fov
            self._projection_matrix = T.perspective_matrix(self.fov, aspect, self.near, self.far)
        else:
            self._projection_matrix = T.orthographic_matrix(self.width, self.height, self.near, self.far)
    
    @classmethod
    def by_size(cls, width, height, pixel_size=1, position=(0,0,0), rotation=(0,0,0), init=True, perspective=False, fov=90, near=0.1, far=100):
        return cls(width//pixel_size, height//pixel_size, pixel_size, position, rotation, init, perspective, fov, near, far)
    
    @classmethod
    def by_png(cls, filename, pixel_size=1, position=(0,0,0), rotation=(0,0,0), dark_spot=(0,0,0,255), light_spot=None, perspective=False, fov=90, near=0.1, far=100):
        pixel_grid = super().by_png(filename, pixel_size, position, rotation, dark_spot, light_spot)
        view_plane = cls(pixel_grid.pixels_x, pixel_grid.pixels_y, pixel_size, position, rotation, True, perspective, fov, near, far)
        view_plane._grid = pixel_grid._grid
        return view_plane

    @property
    def perspective(self):
        return self._perspective
    
    @property
    def near(self):
        return self._near
    
    @property
    def far(self):
        return self._far
    
    @property
    def fov(self):
        if self.perspective:
            return self._fov
        return None
    
    @property
    def projection_matrix(self):
        return self._projection_matrix
    
    def in_active_pixel(self, point=(0, 0)):
        x_index, y_index = [], []
        if point[0] < self.width:
            x_index += [point[0]]
        if point[1] < self.height:
            y_index += [point[1]]
        if point[0] % self.pixel_size == 0 and point[0] >= 1:
            x_index += [point[0]-1]
        if point[1] % self.pixel_size == 0 and point[1] >= 1:
            y_index += [point[1]-1]
        
        for x in x_index:
            for y in y_index:
                if self[int(x // self.pixel_size)][int(y // self.pixel_size)]:
                    return True
        return False

class VoxelGrid:
    def __init__(self, voxels_x, voxels_y, voxels_z, voxel_size=1, centered=False, position=(0,0,0), rotation=(0,0,0), init=True):
        self._voxels_x = int(voxels_x)
        self._voxels_y = int(voxels_y)
        self._voxels_z = int(voxels_z)

        self._voxel_size = voxel_size
        self._centered = centered
        self._position = position
        self._rotation = rotation

        self._model_matrix = self._calc_model_matrix()

        if init:
            self._grid = ones((self.voxels_x, self.voxels_y, self.voxels_z), dtype=bool)
        else:
            self._grid = zeros((self.voxels_x, self.voxels_y, self.voxels_z), dtype=bool)
    
    @classmethod
    def by_size(cls, width, height, depth, voxel_size=1, centered=False, position=(0,0,0), rotation=(0,0,0), init=True):
        return cls(width//voxel_size, height//voxel_size, depth//voxel_size, voxel_size, centered, position, rotation, init)

    def __getitem__(self, key):
        return self._grid[key]
    
    @property
    def voxels_x(self):
        return self._voxels_x

    @property
    def voxels_y(self):
        return self._voxels_y

    @property
    def voxels_z(self):
        return self._voxels_z

    @property
    def voxel_size(self):
        return self._voxel_size
    
    @voxel_size.setter
    def voxel_size(self, voxel_size):
        self._voxel_size = voxel_size
        self._model_matrix = self._calc_model_matrix()
    
    def _calc_model_matrix(self):
        model_matrix = T.model_matrix(self.position, self.rotation, (self.voxel_size,)*3)
        if self._centered:
            centering_matrix = T.translation_matrix((-self.voxels_x/2, -self.voxels_y/2, -self.voxels_z/2))
            model_matrix = matmul(model_matrix, centering_matrix)
        return model_matrix

    @property
    def centered(self):
        return self._centered
    
    @centered.setter
    def centered(self, centered=False):
        self._centered = centered
        self._model_matrix = self._calc_model_matrix()

    @property
    def position(self):
        return self._position
    
    @position.setter
    def position(self, position=(0,0,0)):
        self._position = position
        self._model_matrix = self._calc_model_matrix()

    @property
    def rotation(self):
        return self._rotation
    
    @rotation.setter
    def rotation(self, rotation):
        self._rotation = rotation
        self._model_matrix = self._calc_model_matrix()

    @property
    def model_matrix(self):
        return self._model_matrix
    
    @property
    def width(self):
        return self.voxels_x * self.voxel_size

    @property
    def height(self):
        return self.voxels_y * self.voxel_size

    @property
    def depth(self):
        return self.voxels_z * self.voxel_size
    
    def voxel_vertices(self, voxel_index=(0,0,0)):
        return [(    voxel_index[0],     voxel_index[1],     voxel_index[2]),
                (1 + voxel_index[0],     voxel_index[1],     voxel_index[2]),
                (1 + voxel_index[0], 1 + voxel_index[1],     voxel_index[2]),
                (    voxel_index[0], 1 + voxel_index[1],     voxel_index[2]),
                (    voxel_index[0],     voxel_index[1], 1 + voxel_index[2]),
                (1 + voxel_index[0],     voxel_index[1], 1 + voxel_index[2]),
                (1 + voxel_index[0], 1 + voxel_index[1], 1 + voxel_index[2]),
                (    voxel_index[0], 1 + voxel_index[1], 1 + voxel_index[2])]
    
    def voxel_center(self, voxel_index=(0,0,0)):
        return (1/2 + voxel_index[0], 1/2 + voxel_index[1], 1/2 + voxel_index[2])
    
    def project_and_cut(self, view_plane: ViewPlane):
        mvp_matrix = matmul(matmul(view_plane.projection_matrix, view_plane.view_matrix), self.model_matrix)

        for x in range(self.voxels_x):
            for y in range(self.voxels_y):
                for z in range(self.voxels_z):
                    if self[x][y][z]:
                        p = self.voxel_center((x,y,z))
                        p_screen = T.mvp_transformation(mvp_matrix, p)
                        if T.in_clipping_space(p_screen):
                            screen_pos = T.screen_position(view_plane.width, view_plane.height, p_screen)
                            if view_plane.in_active_pixel(screen_pos):
                                continue
                        self[x][y][z] = False
    
    def dump(self, filename, fill=False):
        def cube_in_obj(verts, faces):
            out = ""
            for vert in verts:
                out += "\nv {vert[0]:.4f} {vert[1]:.4f} {vert[2]:.4f}".format(vert=vert)
            for face in faces:
                out += "\nf {face[0]} {face[1]} {face[2]} {face[3]}".format(face=face)
            return out
        
        def free_neighbor(x,y,z):
            if 0<x and x<self.voxels_x-1 and 0<y and y<self.voxels_y-1 and 0<z and z<self.voxels_z-1:
                return not self[x-1][y][z] or not self[x+1][y][z] or not self[x][y-1][z] or not self[x][y+1][z] or not self[x][y][z-1] or not self[x][y][z+1]
            return True

        verts = {}
        faces = {}
        for x in range(self.voxels_x):
            for y in range(self.voxels_y):
                for z in range(self.voxels_z):
                    if self[x][y][z] and (fill or free_neighbor(x,y,z)):
                        vert_idx = [0]*8
                        for i, v in enumerate(self.voxel_vertices((x,y,z))):
                            if v not in verts:
                                verts[v] = len(verts) + 1
                                vert_idx[i] = len(verts)
                            else:
                                vert_idx[i] = verts[v]

                        faces_tmp = [(vert_idx[0],vert_idx[1],vert_idx[2],vert_idx[3]),
                                     (vert_idx[0],vert_idx[1],vert_idx[5],vert_idx[4]),
                                     (vert_idx[0],vert_idx[4],vert_idx[7],vert_idx[3]),
                                     (vert_idx[1],vert_idx[5],vert_idx[6],vert_idx[2]),
                                     (vert_idx[4],vert_idx[5],vert_idx[6],vert_idx[7]),
                                     (vert_idx[2],vert_idx[3],vert_idx[7],vert_idx[6])]
                        for f in faces_tmp:
                            if f not in faces:
                                faces[f] = None

        verts_proj = []
        for v in verts:
            verts_proj.append(matmul(self.model_matrix, v+(1,)))

        with open(filename, 'w') as file:
            file.write("OBJ File:\n")
            file.write(cube_in_obj(verts_proj, faces))

        