#!/usr/bin/env python3

import sys
import json
from parse_and_load import parse

if __name__ == "__main__": 
    print("Visual Hull - Uni \"Anfängerpraktikum\" Project by Miro Rashid - Summer Semester 2021")
    if len(sys.argv) != 2:
        print("Usage:", sys.argv[0], "<json parameter file>")
        exit()
    
    try:
        parameter_file = open(sys.argv[1], 'r')
        parameters = json.load(parameter_file)
        parse(parameters)
    except OSError as e:
        print("Could not open file:", e)
    except json.JSONDecodeError:
        print("Failed to decode json file.")
    except Exception as e:
        print("An unexpected error has occured:", e)
    exit()