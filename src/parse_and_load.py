from threading import Thread
from grids import ViewPlane, VoxelGrid
from show_voxelgrid import show_voxelgrid
from transformations import fov_of_camera

def pass_or_default(parameters, parameter_name, default_value):
    if parameter_name in parameters:
        return parameters[parameter_name]
    return default_value

def extract_silhouettes(parameters, verbose, multithreading):
    silhouettes_json = pass_or_default(parameters, "silhouettes", [])
    silhouettes_output = []

    def init_silhouette(s):
        filename = pass_or_default(s, "filename", "")
        pixel_size = pass_or_default(s, "pixel_size", 1)
        position = tuple(pass_or_default(s, "position", (0,0,0)))
        rotation = tuple(pass_or_default(s, "rotation", (0,0,0)))
        dark_spot = tuple(pass_or_default(s, "dark_spot", (0,0,0,255)))
        light_spot = pass_or_default(s, "light_spot", None)
        if light_spot is not None:
            light_spot = tuple(light_spot)
        perspective = pass_or_default(s, "perspective", False)
        
        fov = pass_or_default(s, "fov", 90)
        sensor_size = pass_or_default(s, "sensor_size", None)
        focal_length = pass_or_default(s, "focal_length", None)
        if sensor_size is not None and focal_length is not None:
            fov = fov_of_camera(sensor_size, focal_length)
        near = pass_or_default(s, "near", 0.1)
        far = pass_or_default(s, "far", 100)
        silhouettes_output.append(ViewPlane.by_png(filename, pixel_size, position, rotation, dark_spot, light_spot, perspective, fov, near, far))

    if multithreading:
        threads = []
        for s in silhouettes_json:
            t = Thread(target=init_silhouette, args=(s,))
            threads.append(t)
            t.start()
        
        for i, t in enumerate(threads):
            t.join()
            if verbose:
                print(f"Loaded silhouette {i+1}/{len(threads)}")
    else:
        for s in silhouettes_json:
            init_silhouette(s)
            if verbose:
                print(f"Loaded silhouette {i+1}/{len(silhouettes_json)}")
        
    return silhouettes_output

def extract_voxel_grid(parameters):
    voxel_grid_json = pass_or_default(parameters, "voxel_grid", {})
    by_size = pass_or_default(voxel_grid_json, "by_size", False)
    voxels_x = pass_or_default(voxel_grid_json, "voxels_x", 1)
    voxels_y = pass_or_default(voxel_grid_json, "voxels_y", 1)
    voxels_z = pass_or_default(voxel_grid_json, "voxels_z", 1)
    width = pass_or_default(voxel_grid_json, "width", 1)
    height = pass_or_default(voxel_grid_json, "height", 1)
    depth = pass_or_default(voxel_grid_json, "depth", 1)
    centered = pass_or_default(voxel_grid_json, "centered", False)
    voxel_size = pass_or_default(voxel_grid_json, "voxel_size", 1)
    position = tuple(pass_or_default(voxel_grid_json, "position", (0,0,0)))
    rotation = tuple(pass_or_default(voxel_grid_json, "rotation", (0,0,0)))
    
    if by_size:
        return VoxelGrid.by_size(width, height, depth, voxel_size, centered, position, rotation)
    return VoxelGrid(voxels_x, voxels_y, voxels_z, voxel_size, centered, position, rotation)

def parse(parameters):
    verbose = pass_or_default(parameters, "verbose", False)
    multithreading = pass_or_default(parameters, "verbose", True)
    visualize = pass_or_default(parameters, "visualize", True)
    output_filename = pass_or_default(parameters, "output_filename", None)
    fill = pass_or_default(parameters, "fill", False)

    silhouettes = extract_silhouettes(parameters, verbose, multithreading)
    voxel_grid = extract_voxel_grid(parameters)
    
    if verbose:
        print("Created VoxelGrid")

    if multithreading:
        threads = []
        for s in silhouettes:
            t = Thread(target=voxel_grid.project_and_cut, args=(s,))
            threads.append(t)
            t.start()
        for i, t in enumerate(threads):
            t.join()
            if verbose:
                print(f"Cut silhouette {i+1}/{len(threads)} from VoxelGrid")
    else:
        for i, s in enumerate(silhouettes):
            voxel_grid.project_and_cut(s)
            if verbose:
                print(f"Cut silhouette {i+1}/{len(silhouettes)} from VoxelGrid")

    if output_filename is not None:
        voxel_grid.dump(output_filename, fill)
        if verbose:
            print(f"Dumped VoxelGrid to {output_filename}")
    if visualize:
        if verbose:
            print("Starting Visualization...")
        show_voxelgrid(ViewPlane(1280,720,1,(0,0,-50), perspective=True, fov=50, near=1, far=1000), voxel_grid)

    if verbose:
            print("Executed VisualHull sucessfully.")