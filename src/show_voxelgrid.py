import os
os.environ['PYGAME_HIDE_SUPPORT_PROMPT'] = "hide"
from numpy import pi, array, transpose, matmul
import pygame
import transformations as T
from grids import VoxelGrid, ViewPlane

def draw_voxel(screen, vertices, edges = [[1,3,4],[0,2,5],[1,3,6],[0,2,7],[0,5,7],[1,4,6],[2,5,7],[3,4,6]]):
    for v in range(len(vertices)):
        if vertices[v][1]:
            for e in range(len(edges[v])):
                pygame.draw.line(screen, "white", vertices[v][0], vertices[edges[v][e]][0])


def show_voxelgrid(view_plane: ViewPlane, voxel_grid: VoxelGrid):
    pygame.init()
    pygame.display.set_caption("Visual Hull")
    screen = pygame.display.set_mode((view_plane.width,view_plane.height))

    voxel_grid.centered = True
    delta = 0
    while True:
        screen.fill((0,0,0))
        delta += 1
        projected = []
        voxel_grid.rotation = (0,delta,0)

        m_mvp = matmul(matmul(view_plane.projection_matrix, view_plane.view_matrix), voxel_grid.model_matrix)

        for x in range(voxel_grid.voxels_x):
            for y in range(voxel_grid.voxels_y):
                for z in range(voxel_grid.voxels_z):
                    if voxel_grid[x][y][z]:
                        projected = []
                        for p in voxel_grid.voxel_vertices((x,y,z)):
                            p_screen = T.mvp_transformation(m_mvp, p)
                            
                            screen_pos = T.screen_position(view_plane.width, view_plane.height, p_screen)
                            
                            projected.append((screen_pos, T.in_clipping_space(p_screen)))
                        draw_voxel(screen, projected)

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                quit()

        pygame.display.update()
        pygame.time.delay(10)