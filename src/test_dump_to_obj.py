from numpy import array

verts = array([[0,0,0],[1,0,0],[1,1,0],[0,1,0],[0,0,1],[1,0,1],[1,1,1],[0,1,1]])
faces = array([[0,1,2,3],[0,1,5,4],[0,4,7,3],[1,5,6,2],[4,5,6,7],[2,3,7,6]])

f = open("cube.obj","w")
f.write("OBJ File:\n")
for vert in verts:
    f.write("v {vert[0]:.4f} {vert[1]:.4f} {vert[2]:.4f}\n".format(vert=vert))
for face in faces:
    f.write("f ")
    for i in face:
            f.write("%d " % (i + 1))
    f.write("\n")
f.close()