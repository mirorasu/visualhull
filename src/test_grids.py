from numpy import pi, array, transpose, matmul
import transformations as T
import grids
import pygame
from show_voxelgrid import show_voxelgrid
from grids import VoxelGrid, ViewPlane

size = width, height = 1280, 720
near, far = 40, 1000
fov = 70

if __name__ == "__main__":
    view_plane = ViewPlane(width, height, position=(5,1.5,-70), rotation=(0,0,0), perspective=True, fov=fov, near=near, far=far)
    voxel_grid = VoxelGrid(10, 3, 5, 2, False, (0,0,0), (0,0,0))
    show_voxelgrid(view_plane, voxel_grid)



