from grids import VoxelGrid, ViewPlane
from show_voxelgrid import show_voxelgrid

# VoxelGrid
vg = VoxelGrid(8, 12, 10, 1, False, (0,0,0), (0,0,0), True)

# Picture 1:
pic1 = "examples/mini_tree/front.png"
vp1 = ViewPlane.by_png(pic1, 1, (4,6,-1), (0,0,0))
vg.project_and_cut(vp1)

# Picture 2:
pic2 = "examples/mini_tree/side.png"
vp2 = ViewPlane.by_png(pic2, 1, (-1,6,5), (0,-90,0), near=0.1, far=100)
vg.project_and_cut(vp2)

# Print and Visualize:
#print(vg._grid)
vg.dump("examples/mini_tree/output.obj")
show_voxelgrid(ViewPlane(1280, 720, 1, (0,0,-15), perspective=True, fov=70, near=1, far=1000), vg)