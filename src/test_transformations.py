from numpy import pi, array, transpose, matmul
import transformations as T
import pygame

def draw_cube(screen, vertices, edges):
    for v in range(len(vertices)):
        if not vertices[v][1]:
            for e in range(len(edges[v])):
                pygame.draw.line(screen, "white", vertices[v][0], vertices[edges[v][e]][0])

cube1 = [(-0.5,-0.5,-0.5),(-0.5,-0.5,0.5),(-0.5,0.5,-0.5),(-0.5,0.5,0.5),(0.5,-0.5,-0.5),(0.5,-0.5,0.5),(0.5,0.5,-0.5),(0.5,0.5,0.5)]
cube2 = [(0,0,0),(0,0,1),(0,1,0),(0,1,1),(1,0,0),(1,0,1),(1,1,0),(1,1,1)]
cube_edges = [[1,2,4],[0,3,5],[0,3,6],[1,2,7],[0,5,6],[1,4,7],[2,4,7],[3,5,6],[3,5,6]]

size = width,height = 1280,720
m_proj = T.perspective_matrix(17, width/height, 40, 1000)
#m_proj = T.orthographic_matrix(width, height, 40, 1000)

if __name__ == "__main__":
    pygame.init()
    pygame.font.init()
    font = pygame.font.SysFont(None, 24)
    screen = pygame.display.set_mode(size)
    delta = 0
    while True:
        screen.fill((0,0,0))
        delta += 1
        projected = []
        print("delta:",delta)
        m_model = T.model_matrix((0,0,50), (delta,delta,0), (5,5,5))
        m_view = T.view_matrix((0,0,0), (0,0,0))
        m_mvp = matmul(m_proj, matmul(m_view, m_model))

        for p in cube1:
            p_screen = T.mvp_transformation(m_mvp, p)
            
            screen_pos = (width/2 *(1 + p_screen[0]), height/2 *(1- p_screen[1]))
            clip = (abs(p_screen[0]) > 1 or abs(p_screen[1]) > 1 or abs(p_screen[2]) > 1)

            if clip:
                print("clip")
            
            projected.append((screen_pos, clip))

            # draw text:
            screen.blit(font.render(str(p), True, (0, 0, 255)), (screen_pos[0], screen_pos[1] - 24*(p[1]<0)))
            
        draw_cube(screen, projected, cube_edges)

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                quit()

        pygame.display.update()
        pygame.time.delay(100)