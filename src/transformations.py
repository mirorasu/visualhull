from numpy import pi, cos, sin, tan, arctan, radians, array, matmul, linalg, transpose

#left-handed coordinate system with counter-clockwise rotation
def rotation_matrix(rotation=(0,0,0)):
    rad_rotation = radians(rotation)
    cos0, cos1, cos2 = cos(rad_rotation[0]), cos(rad_rotation[1]), cos(rad_rotation[2])
    sin0, sin1, sin2 = sin(rad_rotation[0]), sin(rad_rotation[1]), sin(rad_rotation[2])
    rot_x = array([[1,     0,    0, 0],
                   [0,  cos0, sin0, 0],
                   [0, -sin0, cos0, 0],
                   [0,     0,    0, 1]])
    rot_y = array([[cos1, 0, -sin1, 0],
                   [   0, 1,     0, 0],
                   [sin1, 0,  cos1, 0],
                   [   0, 0,     0, 1]])
    rot_z = array([[ cos2, sin2, 0, 0],
                   [-sin2, cos2, 0, 0],
                   [    0,    0, 1, 0],
                   [    0,    0, 0, 1]])
    return matmul(matmul(rot_z, rot_y), rot_x)

def translation_matrix(position=(0,0,0)):
    return array([[1, 0, 0, position[0]],
                  [0, 1, 0, position[1]],
                  [0, 0, 1, position[2]],
                  [0, 0, 0,          1]])

def scaling_matrix(scale=(1,1,1)):
    return array([[scale[0],        0,        0, 0],
                  [       0, scale[1],        0, 0],
                  [       0,        0, scale[2], 0],
                  [       0,        0,        0, 1]], dtype=float)

def model_matrix(position=(0,0,0), rotation=(0,0,0), scale=(1,1,1)):
    temp = matmul(translation_matrix(position),rotation_matrix(rotation))
    if scale != (1,1,1):
        return matmul(temp,scaling_matrix(scale))
    return temp

def view_matrix(position=(0,0,0), rotation=(0,0,0)):
    return linalg.inv(model_matrix(position, rotation))

def perspective_matrix(fov, aspect, near=0.1, far=100):
    factor = 1 / tan(fov*pi/360)
    return array([[factor/aspect,      0,                     0,                     0],
                  [            0, factor,                     0,                     0],
                  [            0,      0, (far+near)/(far-near), 2*far*near/(near-far)],
                  [            0,      0,                     1,                     0]])

def orthographic_matrix(width, height, near=0.1, far=100):
    return array([[2/width,        0,            0,                      0],
                  [      0, 2/height,            0,                      0],
                  [      0,        0, 2/(far-near), -(far+near)/(far-near)],
                  [      0,        0,            0,                      1]])

def mvp_transformation(mvp_matrix, point):
    point_4d = point + (1,)
    point_4d = matmul(mvp_matrix, point_4d)
    return (point_4d[0]/point_4d[3], point_4d[1]/point_4d[3], point_4d[2]/point_4d[3])

def screen_position(width, height, screen_point=(0,0,0)):
    return (width/2 *(1 + screen_point[0]), height/2 *(1- screen_point[1]))

def in_clipping_space(point):
    return (abs(point[0]) <= 1 and abs(point[1]) <= 1 and abs(point[2]) <= 1)

def fov_of_camera(sensor_size, focal_length):
    return 2 * 180 * arctan(sensor_size / (2*focal_length)) / pi