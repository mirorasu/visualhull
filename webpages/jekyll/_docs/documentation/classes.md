---
title: Classes
permalink: /docs/documentation/classes/
description: Classes and Data Structures used in VisualHull
---

This page describes the primary classes and data structures in the project.

#### Table of Contents
- [Classes](#classes)  
- [Class: PixelGrid](#pixelgrid)  
- [Class: ViewPlane](#viewplane)  
- [Class: VoxelGrid](#voxelgrid)  


<a name="classes"/>

## Classes
There are following main classes, shown in this diagram:

![Class Diagram]({{ "/assets/img/ClassDiagram.drawio.png" | relative_url }} "Class Diagram")

They are all implemented in the file [/src/grids.py](https://gitlab.com/mirorasu/visualhull/-/blob/main/src/grids.py), because the underlying structure is always a 2- or 3-dimensional grid.

---
<br>
<a name="pixelgrid"/>

## Class: PixelGrid
#### Description
Objects of this class contain information about a 2D-grid where individual cells represent pixels.
These information include also its location in the common 3D world.
The Class itself is never used explicitly, rather used in its sub class.

#### Properties
- `pixels_x`, `pixels_y`: integral values, defining the amount of pixels in each direction in regards of the 2D picture, starting in the [top left corner]({{ "/docs/documentation/rationales/#dec3" | relative_url }}).
- `pixel_size`: size of the individual pixels. Because pixels are simplified as squares, this value is equivalent to a pixel's side length. Default: 1.0
- `position`: 3D-vector (in python: tuple) containing the x-y-z-coordinates of the (center of the) grid in the common 3D world. Default: (0,0,0)
- `rotation`: 3D-vector (in python: tuple) containing the rotation of the grid in the common 3D world. Here each component describes its [rotation around the respective axis]({{ "/docs/documentation/rationales/#dec2" | relative_url }}). Default: (0,0,0)
- `view_matrix`: a 4D-matrix for positioning the view resulting from the other properties, used for the MVP-Transformation (model-view-projection matrices) of 4D-vectors representing 3D-points.
- `width`, `height`: size of the whole grid. This property is dependent on the pixel amount in each direction and must be evenly divisible by the pixel_size (will be automatically checked/fitted).
- `grid`: this data structure is a 2D-grid (numpy-array, alternatively quadtree) containing boolean values, which indicate if a pixel belongs to the object's silhouette. The size of the grid will be fixed with the properties `pixels_x` and `pixels_y`. The elements of the grid are accessible by using the `__getitem__`-operator in python (i.e. for example `grid[x]`).

#### Constructors
- `__init__(self, pixels_x, pixels_y, pixel_size=1, position=(0,0,0), rotation=(0,0,0), init=True)`: default constructor, using the actual pixel amounts in the x and y directions.
- `by_size(cls, width, height, pixel_size=1, position=(0,0,0), rotation=(0,0,0), init=True)`: constructor taking width and height of the grid. From this the program will calculate the needed pixel amounts. Warning: if the given size is not evenly divisible by pixels of the given size, the resulting grid will be smaller.
- `by_png(cls, filename, pixel_size=1, position=(0,0,0), rotation=(0,0,0), dark_spot=(0,0,0,255), light_spot=None)`: constructor generating a grid directly from a png file. the pixel amount will be automatically determined. The grid will also be filled with the png-file. For that, if `light_spot` is none, all pixels with the value of `dark_spot` (RGBA (red-green-blue-alpha) values) will be considered part of the silhouette. Else, all pixels with the value of `light_spot` will be considered part of the background, i.e. not silhouette.

#### Methods
- `__getitem__(self, key)`: this sets the python `__getitem__`-operator, so that PixelGrid `grid` can be used as follows: `grid[x][y]` to get the value of the pixel at x, y.
- `pixel_vertices(self, pixel_index=(0,0))`: this statically returns the 4 vertices of a pixel at the given index.

#### System Functions
See System Functions of ViewPlane.

---
<br>
<a name="viewplane"/>

## Class: ViewPlane
#### Description
This class inherits from the super class PixelGrid and extends it by camera or view settings.

#### Properties
- `perspective`: this boolean saves whether to use perspective projection. Else it will use orthographic projection. Default: False
- `near`, `far`: these properties determine where the visible space (clipping space) will be according to its depth. For example, any 3D point which is nearer or further away to the 2D grid will be clipped away. Default: `near` = 0.1, `far` = 100
- `fov`: angle for the field of vision of the camera which captured the silhouette. Only necessary if perspective projection is chosen. Default: 90
- `projection_matrix`: a 4D-matrix for creating a projection resulting from the other properties, used for the MVP-Transformation (model-view-projection matrices) of 4D-vectors representing 3D-points.

#### Constructors
- `__init__(self, pixels_x, pixels_y, pixel_size=1, position=(0,0,0), rotation=(0,0,0), init=True, perspective=False, fov=90, near=0.1, far=100)`: default constructor. See default constructor of ViewPlane
- `by_size(cls, width, height, pixel_size=1, position=(0,0,0), rotation=(0,0,0), init=True, perspective=False, fov=90, near=0.1, far=100)`: constructor taking width and height of the grid. See corresponding constructor of ViewPlane
- `by_png(cls, filename, pixel_size=1, position=(0,0,0), rotation=(0,0,0), dark_spot=(0,0,0,255), light_spot=None, perspective=False, fov=90, near=0.1, far=100)`: constructor generating a grid directly from a png file. See corresponding constructor of ViewPlane

#### Methods
- `in_active_pixel(self, point=(0, 0))`: returns true if the 2D-point (usually a projected 3D-point on the ViewPlane) is contained in a pixel which is marked as part of the silhouette.

#### System Functions
Following System Functions are related to the ViewPlane class:
- [SF: Initialize Binary Grid]({{ "/docs/documentation/system-functions/#sf_initialize_binary_grid" | relative_url }}): a ViewPlane is generated via the `ViewPlane.by_png(...)` constructor from the input silhouettes.
- [SF: Project Voxels]({{ "/docs/documentation/system-functions/#sf_project_voxels" | relative_url }}): the Voxels of the VoxelGrid are projected onto the ViewPlanes. See VoxelGrid.

---
<br>
<a name="voxelgrid"/>

## Class: VoxelGrid
#### Description
This class contains all information about the final 3D object created by the visual hull of the input silhouettes. In the beginning it is a cuboid consisting of cubic voxels (hence the name VoxelGrid), off which single voxels will be cut.

#### Properties
- `voxels_x`, `voxels_y`, `voxels_z`: integral values, defining the amount of voxels in each direction in respect to the x-y-z axes, starting in the [front bottom left corner]({{ "/docs/documentation/rationales/#dec3" | relative_url }}).
- `voxel_size`: size of the individual voxels. Because voxels are simplified as cubes, this value is equivalent to a voxel's side length. Default: 1.0
- `centered`: boolean indicating whether the VoxelGrid's center is located in the origin of the model space. Default: False.
- `position`: 3D-vector (in python: tuple) containing the x-y-z-coordinates of the (front bottom left corner (or center, if centered) of the) grid in the common 3D world. Default: (0,0,0)
- `rotation`: 3D-vector (in python: tuple) containing the rotation of the grid in the common 3D world. Here each component describes its [rotation around the respective axis]({{ "/docs/documentation/rationales/#dec2" | relative_url }}). Default: (0,0,0)
- `model_matrix`: a 4D-matrix resulting for positioning the grid from the other properties, used for the MVP-Transformation (model-view-projection matrices) of 4D-vectors representing 3D-points.
- `width`, `height`, `depth`: size of the whole grid. This property is dependent on the voxel amount in each direction and must be evenly divisible by the voxel_size (will be automatically checked/fitted).
- `grid`: this data structure is a 3D-grid (numpy-array, alternatively octree) containing boolean values, which indicate if a voxel belongs to the final object, i.e. when it is not cut of by not being part of a silhouette. The size of the grid will be fixed with the properties `voxels_x`, `voxels_y`, and `voxels_z`. The elements of the grid are accessible by using the `__getitem__`-operator in python (i.e. for example `grid[x]`).

#### Constructors
- `__init__(self, voxels_x, voxels_y, voxels_z, voxel_size=1, centered=False, position=(0,0,0), rotation=(0,0,0), init=True)`: default constructor, using the actual voxel amounts in the x, y, z directions.
- `by_size(cls, width, height, depth, voxel_size=1, centered=False, position=(0,0,0), rotation=(0,0,0), init=True)`: constructor taking width, height, and depth of the grid. From this the program will calculate the needed pixel amounts. Warning: if the given size is not evenly divisible by voxel of the given size, the resulting grid will be smaller.

#### Methods
- `__getitem__(self, key)`: this sets the python `__getitem__`-operator, so that VoxelGrid `grid` can be used as follows: `grid[x][y][z]` to get the value of the pixel at x, y, z.
- `_calc_model_matrix(self)`: this private method will (re)calculate the model matrix in the case of a changed rotation, position, or centering.
- `voxel_vertices(self, voxel_index=(0,0,0))`: this statically returns the 8 vertices of a voxel at the given index.
- `voxel_center(self, voxel_index=(0,0,0))`: this statically returns the center of a voxel at the given index.
- `project_and_cut(self, view_plane: ViewPlane)`: this projects all active voxels (actually their centers) onto the given ViewPlane via MVP-Transformation. If the voxel is **not** contained in an active pixel, which is checked via `ViewPlane.in_active_pixel(self, point)`, the voxel will be cut off (by setting it to False).
- `dump(self, filename, fill=False)`: this method saves the VoxelGrid into a file in the [Wavefront OBJ format]({{ "/docs/documentation/rationales/#dec4" | relative_url }}). For this only the active voxels are regarded. By the `fill` argument, it will be set whether to include "useless" voxels which are surrounded by other neighboring voxels and are therefore invisible.


#### System Functions
Following System Functions are related to the VoxelGrid class:
- [SF: Create Voxel Grid]({{ "/docs/documentation/system-functions/#sf_create_voxel_grid" | relative_url }}): a VoxelGrid is generated via a constructor.
- [SF: Project Voxels]({{ "/docs/documentation/system-functions/#sf_project_voxels" | relative_url }}): the Voxels of the VoxelGrid are projected onto the ViewPlanes via `VoxelGrid.project_and_cut(self, view_plane)`
- [SF: Dump To File]({{ "/docs/documentation/system-functions/#sf_dump_to_file" | relative_url }}): the VoxelGrid is dumped to a file via `VoxelGrid.dump(self, filename, fill)`.
