---
title: Rationales
permalink: /docs/documentation/rationales/
description: Rationales to some Decisions
---

This page collects a number of rationales concerning some decision makings for this project.

#### Table of Contents
- [Decision: Usage of Python and NumPy](#dec1)  
- [Decision: Left-Handed Coordinate System](#dec2)  
- [Decision: 2D and 3D Coordinate Directions of Grids](#dec3)  
- [Decision: Output File Format Wavefront .obj](#dec4)  

---
<br>
<a name="dec1"/>

## Decision: Usage of Python and NumPy
[Python](https://www.python.org/) is a good programming language for scientific purposes, but it might be not the best choice for rendering, as it is a scripting language and takes more calculation time than compiled code.

Nonetheless I chose [Python](https://www.python.org/) as there is no real need for a in-time calculation because we at most need one 3D Model to be generated in a run, rather than multiple frames to be rendered.

To compensate higher storage usage and backend calculation time (e.g. matrix multiplication), there will be the usage of [NumPy](https://numpy.org/) that provides data structures (e.g. array/matrix) and algorithms that might have a lower run time than pure [Python](https://www.python.org/) Code.

This is based by the fact, that [NumPy](https://numpy.org/) uses precompiled C Code in background.

---
<br>
<a name="dec2"/>

## Decision: Left-Handed Coordinate System
Let Left-Handed Coordinate System be defined as follows: If the camera/view has no rotation and is at the origin (x=0,y=0,z=0), it will face positive z-direction, so that the coordinates that will be visible are positive.
    
Furthermore, positive x-direction will be to the right and positive y-direction will be upwards. (Compared to Right-Handed Coordinate Systems: here the camera/view is facing negative z-direction).

The naming originates in the left hand, where you put out your middle finger (x-axis), thumb (y-axis) and index finger (z-axis) such that they are naturally orthogonal to each other.
    
Additionally the rotations around the axes will be according the "right hand rule" (as it is standard in mathematics), so that if you roll your fingers of your right hand and point your thumb in the axis direction, the rotation will occur in the direction of your rolled up fingers.

Please keep in mind that some 3D-engines might use another Coordinate System that might be in conflict with this. For example, [Blender](https://www.blender.org/) uses a Right-Handed Coordinate System where the z-axis is pointing upwards, the x-axis to the right and the y-axis forwards. This has the effect that imported models have to be flipped vertically

---
<br>
<a name="dec3"/>

## Decision: 2D and 3D Coordinate Directions of Grids
This decision defines the coordinates for instances of the Grid Classes `PixelGrid` and `VoxelGrid`

The 2D PixelGrid will be column major and furthermore getting the element `grid[x][y]` means starting from the upper left corner, to go x to the right and y down.

This convention is due to the fact, that screens also use this indexing. The png file format is row major though, therefore the picture has to be loaded transposed in to the array.

Counter to that, the 3D VoxelGrid is starting at the front bottom left corner and getting the element `grid[x][y][z]` means going x to the right, y up and z forwards, just like the coordinate system.

---
<br>
<a name="dec4"/>

## Decision: Output File Format Wavefront .obj
The .obj file format is good for custom polygon forms and not only limited to triangles.

As the resulting 3D Model is consisting of Voxels, which are cubes, this format is useful, because the voxels can easily be split up into its square faces.

Another advantage of this file format is its easy to debug due to its ascii format.

[Blender](https://www.blender.org/) and other 3D Processing Programs are supporting this file format.