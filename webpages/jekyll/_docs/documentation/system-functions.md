---
title: System Functions
permalink: /docs/documentation/system-functions/
description: System Functions and Processes Documentation
---

This page describes the System Functions of the project and the execution process.
System Functions are generally supposed to be kept independent from implementation as much as possible.
Therefore the actual implementation might deviate from the definitions in the System Functions.

#### Table of Contents
- [Processes](#processes)  
- [SF: Parse / Load](#sf_parse_load)  
- [SF: Initialize Binary Grid](#sf_initialize_binary_grid)  
- [SF: Create Voxel Grid](#sf_create_voxel_grid)  
- [SF: Project Voxels](#sf_project_voxels)  
- [SF: Dump To File](#sf_dump_to_file)  


<a name="processes"/>

## Processes
The general execution processes are described via following diagram:

![Processes Diagram]({{ "/assets/img/Processes.drawio.png" | relative_url }} "Processes Diagram")

The main elements are System Functions which all have a distinct assignment, input, and output and will be described on this page.

The outline procedure of a run will be as follows:
1. **Parse / Load** reads the input parameters and loads the silhouette pictures mentioned therein
2. **Initialize Binary Grid** creates a 2D Grid of boolean values for each picture to indicate which pixels are part of the silhouette
3. **Create Voxel Grid** initializes a 3D Grid of boolean values which will indicate which voxels are part of the resulting 3D object
4. **Project Voxel** projects all voxels onto the 2D Grids generated in _step 2_ and cuts those voxels away whose projection is not part of a silhouette
5. **Dump To File** converts the 3D Grid into a readable file format for 3D Models, ready for exportation

---
<br>
<a name="sf_parse_load"/>

## SF: Parse / Load
#### Implementation
```python
parse_and_load.parse(parameters)
```
- **Status of Implementation:** done
- **Status of Test:** tested in `main.py`
- **Deviation to SF:** This method executes all other required SF and is in the role of a root process

#### Input
- **Parameters**, which describe the properties of a run → `parameters` (dict, converted from json-file)

#### Output
- (optional) **Window**, which shows the generated 3D Model → iff `visualize` in json is set to `true`
- (optional) **Output File**, which contains the generated 3D Model → iff `output_filename` in json is not `null`

#### Prerequisites
- Pictures/Silhouettes mentioned in parameters exist

#### Run
- **Always**; once at start

#### Function
This System Function brings all methods together and parses the configuration and the parameters for the generation of the 3D Model.

With these parameters, the parser will move on to load the mentioned silhouettes that are contained in picture files.

Then, it will generate all objects and performs all operations needed and will output the result.

---
<br>
<a name="sf_initialize_binary_grid"/>

## SF: Initialize Binary Grid
#### Implementation
```python
PixelGrid.by_png(cls, filename, pixel_size, position, rotation, dark_spot, light_spot)
ViewPlane.by_png(cls, filename, pixel_size, position, rotation, dark_spot, light_spot, perspective, fov, near, far)
```
- **Status of Implementation:** done
- **Status of Test:** tested in `test_grids.py` and `test_projector.py`
- **Deviation to SF:** The constructors are taking more inputs as they contain detailed information about the cameras' perspectives

#### Input
- **Picture**, representing a silhouette → `filename` (png-file)
- **Color**, to stay in 3D-Model → `dark_spot` (RGBA-tuple)
- **Color**, to carve from 3D-Model → `light_spot` (RGBA-tuple)

#### Output
- **Grid**, consisting of silhouette → Constructed `PixelGrid` or `ViewPlane` (boolean valued 2D-array)

#### Prerequisites
- Either the silhouette is in the dark spot color or everything else is in the light spot color

#### Run
- **Always**; for each input picture
- Parallelizable: thread per picture

#### Function
This System Function loads the input of a picture (i.e. png picture file) into a consistent data type within this project.
    
The data type is called `grids.PixelGrid` and contains a 2D-Array of booleans, true for dark spots and false for light spots of a silhouette.

Analogous to the screen coordinate system, this 2D-Array is column major and starts in the top left corner of the picture.

Therefore the picture is loaded in a transposed manner.

---
<br>
<a name="sf_create_voxel_grid"/>

## SF: Create Voxel Grid
#### Implementation
```python
grids.VoxelGrid.__init__(self, voxels_x, voxels_y, voxels_z, voxel_size, position, rotation, init)
grids.VoxelGrid.by_size(cls, width, height, depth, voxel_size, position, rotation, init)
```
- **Status of Implementation:** done
- **Status of Test:** tested in `test_grids.py` and `test_projector.py`
- **Deviation to SF:** The inputs for Voxel Size, Position, Rotation, and Initial Value are optional as they have default values. The constructor `by_size` takes the actual dimensions of the Grid opposed to the amount of voxels per direction.

#### Input
- **Number of Voxels** in each direction → `voxels_x, voxels_y, voxels_z` (int)
- **Size of a Voxel** → `voxel_size` (int)
- **Position** in World Space → `position` (triplet containing x,y,z coordinate)
- **Rotation** in Model Space → `rotation` (triplet containing rotation in degrees around x,y,z axes)
- **Initialization** of the Elements of the Array → `init` (boolean value)

#### Output
- **Grid**, consisting of voxels → Constructed `VoxelGrid` (boolean valued 3D-Array)

#### Prerequisites
- Sizes are known and comply to the input silhouettes

#### Run
- **Always**; once per run

#### Function
This System Function creates a 3D Grid for the final 3D Model, which consists of voxels.

The data type is called `grids.VoxelGrid` and contains a 3D-Array of booleans, indicating whether a voxel is part of the model.

The array used describes the voxels from the front-bottom-left corner with their indices being the relative pre-transformed positions in x, y, and z directions.

---
<br>
<a name="sf_project_voxels"/>

## SF: Project Voxels
#### Implementation
```python
VoxelGrid.project_and_cut(self, view_plane)
```
- **Status of Implementation:** done
- **Status of Test:** tested in `test_transformations.py` and `test_projector.py`
- **Deviation to SF:** None.

#### Input
- **Grid** to carve the voxels out → `self` (VoxelGrid)
- **Plane of View** with marked pixels as part of a silhouette → `view_plane` (ViewPlane)

#### Output
- **None**; Changes are made to the input Voxel Grid.

#### Prerequisites
- The input pictures have to be initialized and loaded into the view planes.

#### Run
- **Always**; for each input picture (view plane)
- Parallelizable: thread per view plane

#### Function
This System Function projects each voxel (in particular its center point) in the voxel grid onto a viewing plane.

Then, it will look whether the voxel lies in a silhouette pixel and depending on that, it will deactivate the voxel.

This has the effect that every voxel that does not project onto a pixel of the actual silhouette, will be cut out.

---
<br>
<a name="sf_dump_to_file"/>

## SF: Dump To File
#### Implementation
```python
VoxelGrid.dump(self, filename, fill)
```
- **Status of Implementation:** done
- **Status of Test:** tested in `test_dump_to_obj.py` and `test_projector.py`
- **Deviation to SF:** None.

#### Input
- Name of the **Output File** → `filename`

#### Output
- **None**; VoxelGrid 3D Model is saved in the given file

#### Prerequisites
- The VoxelGrid is loaded fully and is in its final form

#### Run
- Iff filename is not empty; once per run

#### Function
This System Function converts the result 3D-model (here: VoxelGrid) into a file, which is saved on the file system.

The file format is "Wavefront .obj".