---
title: Main Parameters
permalink: /docs/parameters/
description: Main Parameters in JSON File
---

There are certain parameters that can be set to configure the execution of the VisualHull program.
To run the program, you can enter following line into your shell/cmd:

```
./src/main.py <json parameter file>
```

or if that does not work, try:

```
python3 ./src/main.py <json parameter file>
```

Make sure your current working directory is that of the git-project folder.

The argument `<json parameter file>` has to be given for every run or else the program cannot execute.

In this file you can set your parameters just as you want.

If you have no clue how to create such a file, you can see the examples as reference or even use the template [examples/parameters.json](https://gitlab.com/mirorasu/visualhull/-/blob/main/examples/parameters.json) file and start from there.

Here is a list of the main parameters implemented:

- [verbose](#verbose)
- [multithreading](#multithreading)
- [output_filename](#output_filename)
- [fill](#fill)
- [visualize](#visualize)
- [silhouettes](#silhouettes)
    - [Silhouette Parameter: filename]({{ "/docs/parameters/silhouette/" | relative_url }}#filename)
    - [Silhouette Parameter: pixel_size]({{ "/docs/parameters/silhouette/" | relative_url }}#pixel_size)
    - [Silhouette Parameter: position]({{ "/docs/parameters/silhouette/" | relative_url }}#position)
    - [Silhouette Parameter: rotation]({{ "/docs/parameters/silhouette/" | relative_url }}#rotation)
    - [Silhouette Parameter: dark_spot]({{ "/docs/parameters/silhouette/" | relative_url }}#dark_spot)
    - [Silhouette Parameter: light_spot]({{ "/docs/parameters/silhouette/" | relative_url }}#light_spot)
    - [Silhouette Parameter: perspective]({{ "/docs/parameters/silhouette/" | relative_url }}#perspective)
    - [Silhouette Parameter: fov]({{ "/docs/parameters/silhouette/" | relative_url }}#fov)
    - [Silhouette Parameter: sensor_size]({{ "/docs/parameters/silhouette/" | relative_url }}#sensor_size)
    - [Silhouette Parameter: focal_length]({{ "/docs/parameters/silhouette/" | relative_url }}#focal_length)
    - [Silhouette Parameter: near]({{ "/docs/parameters/silhouette/" | relative_url }}#near)
    - [Silhouette Parameter: far]({{ "/docs/parameters/silhouette/" | relative_url }}#far)
- [voxel_grid](#voxel_grid)
    - [VoxelGrid Parameter: by_size]({{ "/docs/parameters/voxelgrid/" | relative_url }}#by_size)
    - [VoxelGrid Parameter: voxels_x, voxels_y, and voxels_z]({{ "/docs/parameters/voxelgrid/" | relative_url }}#voxels)
    - [VoxelGrid Parameter: width, height, and depth]({{ "/docs/parameters/voxelgrid/" | relative_url }}#size)
    - [VoxelGrid Parameter: voxel_size]({{ "/docs/parameters/voxelgrid/" | relative_url }}#voxel_size)
    - [VoxelGrid Parameter: centered]({{ "/docs/parameters/voxelgrid/" | relative_url }}#centered)
    - [VoxelGrid Parameter: position]({{ "/docs/parameters/voxelgrid/" | relative_url }}#position)
    - [VoxelGrid Parameter: rotation]({{ "/docs/parameters/voxelgrid/" | relative_url }}#rotation)

---

### verbose
- **Type:** Boolean (_true_ or _false_)
- **Default value:** _false_

If this parameter is set to _true_, the execution of the VisualHull program will print out progress steps that are finished to stdout.

---

### multithreading
- **Type:** Boolean (_true_ or _false_)
- **Default value:** _true_

This parameter will determine whether to run parallelizable steps of the program in separate threads.
If you have multiple cpu cores, this might save you some execution time.

---

### output_filename
- **Type:** String or _null_
- **Default value:** _null_

If this parameter is kept empty or set to null, the program won't save the resulting 3D model after execution.

Else it will save the result in the given file in the Wavefront .obj format.

---

### fill
- **Type:** Boolean (_true_ or _false_)
- **Default value:** _false_

This parameter decides whether to fill in voxels that will not be visible, because they are blocked by neighboring voxels, into the output file.

Generally speaking, if fill is set to true, the output file will be bigger (if there are enclosed voxels) even though these voxels are not relevant from the outside of the model.

---

### visualize
- **Type:** Boolean (_true_ or _false_)
- **Default value:** _true_

This parameter decides, whether to show a window that shows and rotates the resulting visual hull after creation.

---

### silhouettes
- **Type:** Array of Silhouettes
- **Default value:** []

This array collects all silhouettes, each with their own parameters.

The parameters for each silhouette are described on the next page: [Silhouette Parameters]({{ "/docs/parameters/silhouette/" | relative_url }})

---

### voxel_grid
- **Type:** VoxelGrid (dict/map in JSON)
- **Default value:** {}

This map/object contains parameters that describe the VoxelGrid which will be the canvas for the resulting 3D model.

The parameters for the VoxelGrid are described here: [VoxelGrid Parameters]({{ "/docs/parameters/voxelgrid/" | relative_url }})