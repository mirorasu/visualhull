---
title: ↳ Silhouette Parameters
permalink: /docs/parameters/silhouette/
description: Silhouette Parameters in JSON File
---

Here is a list of parameters that can be set for each silhouette in the parameter json file as elements in the array `silhouettes`:

- [filename](#filename)
- [pixel_size](#pixel_size)
- [position](#position)
- [rotation](#rotation)
- [dark_spot](#dark_spot)
- [light_spot](#light_spot)
- [perspective](#perspective)
- [fov](#fov)
- [sensor_size](#sensor_size)
- [focal_length](#focal_length)
- [near](#near)
- [far](#far)

---

### filename
- **Type:** String
- **Default value:** ""

This parameter is the filename to the .png-file containing the silhouette.

Make sure to give the path relative to your current working directory and not relative to the main.py script or the parameters json file.

---

### pixel_size
- **Type:** Number (integer or floating point)
- **Default value:** 1

This parameter scales the pixels of the silhouettes by the given factor.

---

### position
- **Type:** 3D-Vector (Array with 3 elements)
- **Default value:** [0,0,0]

This parameter sets the position of the camera/view for the silhouette.

The first element for the x-component, second for y-component and third for z-component

---

### rotation
- **Type:** 3D-Vector (Array with 3 elements)
- **Default value:** [0,0,0]

This parameter sets the rotation of the camera/view for the silhouette.

The first element for rotation around x-axis, second around y-axis and third around z-axis.

The unit here is in degrees (°).

---

### dark_spot
- **Type:** 8-bit RGBA Color Vector (Array with 4 elements; red-green-blue-alpha)
- **Default value:** [0,0,0,255]

This parameter sets which color belongs to the silhouette.
Default value is black color with full alpha (no transparency).

If the parameter `light_spot` is set and not null, this parameter will have no impact, as the light_spot color will already decide, which color does **not** belong to the silhouette.

---

### light_spot
- **Type:** null or 8-bit RGBA Color Vector (Array with 4 elements; red-green-blue-alpha)
- **Default value:** _null_

This parameter sets which color does **not** belong to the silhouette.
Default value is null, because if this value is set, then the parameter `dark_spot` would have no impact on the execution.

If your background (not part of silhouette) is white, you can set this parameter to [255,255,255,255] (white with no transparency)

---

### perspective
- **Type:** Boolean (_true_ or _false_)
- **Default value:** _false_

This parameter sets whether the silhouette is in a perspective projection (e.g. camera image, eyes) or in orthographic projection (set to false).

---

### fov
- **Type:** Number (integer or floating point)
- **Default value:** 90

This parameter sets the field-of-view of the silhouette view.
The unit is in degrees (°).

This is only important, when the projection type is perspective, because orthographic projections do not need an fov.

Also keep in mind that this parameter won't be used, if the parameters `sensor_size` **and** `focal_length` are set and not null. Else, the fov will be calculated by the other two parameters.

---

### sensor_size
- **Type:** Number (integer or floating point)
- **Default value:** _null_

This parameter sets the sensor size of the silhouette view/camera.

This parameter will only be used, if projection is perspective **and** the parameter `focal_length` is set and not null.
Then the field-of-view will be automatically calculated.

---

### focal_length
- **Type:** Number (integer or floating point)
- **Default value:** _null_

This parameter sets the focal length of the silhouette view/camera.

This parameter will only be used, if projection is perspective **and** the parameter `sensor_size` is set and not null.
Then the field-of-view will be automatically calculated.

---

### near
- **Type:** Number (integer or floating point)
- **Default value:** 0.1

This parameter sets the relative position of the near-plane to the view/camera.

That means that 3D objects that have a "nearer" relative position than the near-plane (i.e. smaller than the near parameter), will be cut off from the view.

---

### far
- **Type:** Number (integer or floating point)
- **Default value:** 0.1

This parameter sets the relative position of the far-plane to the view/camera.

That means that 3D objects that have a "further" relative position than the far-plane (i.e. bigger than the near parameter), will be cut off from the view.