---
title: ↳ VoxelGrid Parameters
permalink: /docs/parameters/voxelgrid/
description: VoxelGrid Parameters in JSON File
---

Here is a list of parameters that can be set for a VoxelGrid in the parameter json file as the sub-object `voxel_grid`:

- [by_size](#by_size)
- [voxels_x, voxels_y, and voxels_z](#voxels)
- [width, height, and depth](#size)
- [voxel_size](#voxel_size)
- [centered](#centered)
- [position](#position)
- [rotation](#rotation)

---

### by_size
- **Type:** Boolean (_true_ or _false_)
- **Default value:** _false_

This parameter will decide, whether to build the VoxelGrid by its amount of voxels (i.e. with `voxels_x`, `voxels_y`, and `voxels_z`) or by its size (i.e. with `width`, `height`, and `depth`).

If it is build by amount of voxels, the size will be determined by scaling it by the factor of the single voxel size (`voxel_size`).

If it is build by size, the amount of voxels will be determined by dividing it by the same factor (`voxel_size`). Mind that this is an integer division, that means that the resulting size might be smaller than intended, because per convention we do not want to have non-cubic voxels.

---
<a name="voxels">

### voxels_x, voxels_y, and voxels_z
- **Type:** Integers
- **Default value:** 1

These are separate parameters and set the amount of voxels in each direction. E.g., `'voxels_x': 10` will set 10 voxels in to the x direction, such that the VoxelGrid's width will be 10 voxels.

The usage of these parameters is determined by `by_size` (should be set to _false_).

---
<a name="size">

### width, height, and depth
- **Type:** Numbers (integer or floating point)
- **Default value:** 1

These are separate parameters and set the size of the VoxelGrid in each dimension.

The usage of these parameters is determined by `by_size` (should be set to _true_).

---

### voxel_size
- **Type:** Number (integer or floating point)
- **Default value:** 1

This parameter determines the size of the voxels. Because a voxel is defined here as a cube, we start from a 1x1x1 cube and scale it by this parameter.

---

### centered
- **Type:** Boolean (_true_ or _false_)
- **Default value:** _false_

This parameter centers the VoxelGrid in model space such that its center is at origin before further translation and rotation.

---

### position
- **Type:** 3D-Vector (Array with 3 elements)
- **Default value:** [0,0,0]

This parameter sets the position of the VoxelGrid.

The first element for the x-component, second for y-component and third for z-component

Note that the VoxelGrid is not centered at the origin by default but the first voxel (grid[0][0][0]) has its front bottom left corner at the origin.

---

### rotation
- **Type:** 3D-Vector (Array with 3 elements)
- **Default value:** [0,0,0]

This parameter sets the rotation of the VoxelGrid.

The first element for rotation around x-axis, second around y-axis and third around z-axis.

The unit here is in degrees (°).

Note that the VoxelGrid is not centered at the origin by default but the first voxel (grid[0][0][0]) has its front bottom left corner at the origin.