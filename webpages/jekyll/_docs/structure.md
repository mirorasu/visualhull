---
title: Project Structure
permalink: /docs/structure/
description: Structure of the VisualHull Project
---

## Project Structure
All files of the VisualHull Project are maintained on the [git repository](https://gitlab.com/mirorasu/visualhull).

Here is a list of all files and directories in the root folder. Some of them are described in more detail below.

- **/.vscode/**: Launch configurations for python debugging in VSCode/[VSCodium](https://vscodium.com/).
- **/documentation/**: See [below](#documentation).
- **/examples/**: See [below](#examples).
- **/icon/**: Icon and Background images, used as the project icon and on the [index page]({{ "/" | relative_url }}).
- **/slides/**: Slides used in the project presentation, held on 25.10.2021.
- **/src/**: See [below](#src).
- **/webpages/jekyll/**: Uncompiled webpage files, which will be generated with jekyll into these project pages.
- **/.gitignore**: Generic ignore file for ignoring python cache files.
- **/.gitlab-ci.yml**: File containing CI-configuration for automatic generation of the GitLab Pages onto [mirorasu.gitlab.io/visualhull/](https://mirorasu.gitlab.io/visualhull/).
- **/LICENSE**: MIT License for free open source projects.
- **/README.md**: README file for the face of the git repository.


---
<br>
<a name="documentation"/>

## Directory: /documentation/
This directory contains some documentation files e.g. diagrams extending the coding.
These are also fully available on the webpages here:
- [System Functions]({{ "/docs/documentation/system-functions/" | relative_url }})
- [Classes]({{ "/docs/documentation/classes/" | relative_url }})
- [Rationales]({{ "/docs/documentation/rationales/" | relative_url }})


---
<br>
<a name="examples"/>

## Directory: /examples/
This directory contains all example files like source files, parameters, and resulting output files.
These are fully listed here:

[About the Examples]({{ "/docs/documentation/examples/preface/" | relative_url }})


---
<br>
<a name="src"/>

## Directory: /src/
This directory contains all python coding for the visual hull creation program and some tests:

### grids.py
This file contains the essential classes *PixelGrid*, *ViewPlane*, and *VoxelGrid* used in the process. These are detailed in full detail in the documentation for [classes]({{ "/docs/documentation/classes/" | relative_url }}).

### main.py
This file is the main program.
For execution of the program, this file should be executed with a JSON file, containing parameters, as the argument.
Theses parameters are described in [Main Parameters]({{ "/docs/parameters/" | relative_url }}).

### parse_and_load.py
This file parses the parameters of the given JSON file.
Then it will load the silhouette pictures mentioned in the parameters.
After that, the program will run the necessary steps to create the resulting model and exporting it to an output file.
For more details, see the [System Functions]({{ "/docs/documentation/rationales/" | relative_url }}).

### show_voxelgrid.py
This file optionally generates a window containing the resulting model after creation.
For rendering, it is using the self-coded transformations from transformations.py.
For the window, it is using the PyGame-Library.

### transformations.py
This file contains functions that calculate

- linear transformations like
    - Rotation-Matrix
    - Translation-Matrix
    - Scaling-Matrix
    - in combination: Model- and View-Matrix
    - Projection-Matrices: for perspective and orthographic projections
    - screen position from NDC (normalized device coordinates; [-1,1]-intervals for each axis): translation
- full MVP-Transformation (model-view-projection matrices) with subsequent division by "w-coordinate" for depth creation
- if a point in NDC is contained in the clipping space
- FOV (field-of-view) of camera from sensor size and focal length

## Tests:

### test_dump_to_obj.py
This test is testing the 3D model exportation to the Wavefront obj format, to see if the used format is parsable and syntactically and semantically correct.

### test_grids.py
This test is testing whether the generated Model-, View-, and Projection-Matrices in ViewPlane and VoxelGrid are producing correct rendering.

### test_projector.py
This test is testing the `VoxelGrid.project_and_cut(view_plane)` function to see if the right model is being produced.
For this, the example [Mini Tree]({{ "/examples/mini-tree/" | relative_url }}) was created.

### test_transformations.py
This test is testing the transformations in transformations.py by rendering a cube and showing it in a window produced by the PyGame-Library.