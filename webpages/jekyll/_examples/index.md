---
title: About the Examples
permalink: /examples/preface/
redirect_from: /examples/index.html
description: Information about the VisualHull Project
---

This tab section is a collection of examples that were produced by the VisualHull program.
These are in some cases *creations* by fictional silhouettes or *recreations* using produced silhouettes of an existing object whether modelled digitally or captured from real-life.

All silhouettes, original files, parameters, result outputs, etc. are located in the git-repo at [/examples/](https://gitlab.com/mirorasu/visualhull/-/blob/main/examples).

## List of examples

Click on an picture or an entry in the left menu to get more details:

<div class="row">
  <div class="col-lg-4 col-sm-6">
    <div class="thumbnail">
      <div class="image">
        <a href="{{ "/examples/mini-house/" | relative_url }}"><img src="{{ "/assets/img/examples/mini_house_result.gif" | relative_url }}" class="img-responsive" alt="Mini House Result"></a>
      </div>
      <div class="caption">
        <h3>Mini House</h3>
        <p>Test generation of a pixelated house</p>
      </div>
    </div>
  </div>
  <div class="col-lg-4 col-sm-6">
    <div class="thumbnail">
      <div class="image">
        <a href="{{ "/examples/mini-tree/" | relative_url }}"><img src="{{ "/assets/img/examples/mini_tree_result.gif" | relative_url }}" class="img-responsive" alt="Mini Tree Result"></a>
      </div>
      <div class="caption">
        <h3>Mini Tree</h3>
        <p>Test generation of a pixelated tree</p>
      </div>
    </div>
  </div>
  <div class="col-lg-4 col-sm-6">
    <div class="thumbnail">
      <div class="image">
        <a href="{{ "/examples/landscape/" | relative_url }}"><img src="{{ "/assets/img/examples/landscape_result.gif" | relative_url }}" class="img-responsive" alt="Landscape Result"></a>
      </div>
      <div class="caption">
        <h3>Landscape</h3>
        <p>Randomly generated landscape</p>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-lg-4 col-sm-6" >
    <div class="thumbnail">
      <div class="image">
        <a href="{{ "/examples/minerva/" | relative_url }}"><img src="{{ "/assets/img/examples/minerva_result.gif" | relative_url }}" class="img-responsive" alt="Minerva Result"></a>
      </div>
      <div class="caption">
        <h3>Minerva</h3>
        <p>Reconstruction of a model of the Roman goddess of wisdom</p>
      </div>
    </div>
  </div>
  <div class="col-lg-4 col-sm-6">
    <div class="thumbnail">
      <div class="image">
        <a href="{{ "/examples/sphinx/" | relative_url }}"><img src="{{ "/assets/img/examples/sphinx_result.gif" | relative_url }}" class="img-responsive" alt="Sphinx Result"></a>
      </div>
      <div class="caption">
        <h3>Sphinx</h3>
        <p>Reconstruction of a real-life object depicting the Great Sphinx of Giza</p>
      </div>
    </div>
  </div>
</div>