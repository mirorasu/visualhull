---
title: Landscape
permalink: /examples/landscape/
description: Landscape Example
---

This example is a randomly generated landscape that was converted into a VoxelGrid.
The resulting model is used as the mountainous region in the picture on the [front page]({{ site.baseurl }}/).

### Source Model
The source model can be found in the git project folder [/examples/landscape/silhouettes/model.blend](https://gitlab.com/mirorasu/visualhull/-/blob/main/examples/landscape/silhouettes/model.blend).
The landscape was generated randomly with [Blender](https://www.blender.org/) and the included Add-On Add Mesh: [A.N.T.Landscape](https://docs.blender.org/manual/en/latest/addons/add_mesh/ant_landscape.html):
![Landscape Source Model]({{ "/assets/img/examples/landscape.gif" | relative_url }} "Landscape Source Model")

### Silhouettes
See here: [/examples/landscape/silhouettes/*.png](https://gitlab.com/mirorasu/visualhull/-/blob/main/examples/landscape/silhouettes)

The silhouette pictures were taken in Blender in 12 rotations at 30°-steps around the y-axis (in Blender: z-axis).
The positions of the camera were adjusted manually so that no significant floating point errors occur.

### Parameters
See here: [/examples/landscape/parameters_full.json](https://gitlab.com/mirorasu/visualhull/-/blob/main/examples/landscape/parameters_full.json)

The positions and rotations for the camera settings were recorded manually.
Unlike the Blender meter units, I chose to use decimeters (10 dm = 1 m).

The field-of-view (fov) of 40° was preset in the Blender project and added to the parameters.

The cube size was set to 0.5, the smaller the number, the more details can be captured.
Keep in mind that a higher voxel count means longer computing time.

### Result
After running the VisualHull code with the given parameters json-file, the resulting model got created:
![Landscape Result]({{ "/assets/img/examples/landscape_result.gif" | relative_url }} "Landscape Result")