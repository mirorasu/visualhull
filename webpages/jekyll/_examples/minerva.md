---
title: Minerva
permalink: /examples/minerva/
description: Minerva Example
---

This example is a model of a bust of Minerva, the Roman goddess of wisdom etc., that was converted into a VoxelGrid.

### Source Model
The original model ["Minerva"](https://skfb.ly/CCJt) was created by _Macías_ and is licensed under [Creative Commons Attribution CC-BY](http://creativecommons.org/licenses/by/4.0/).
The Source Model can be found at the link above or in the git project folder [/examples/minerva/source_model/Minerva.obj](https://gitlab.com/mirorasu/visualhull/-/blob/main/examples/minerva/source_model/Minerva.obj).

![Minerva Source Model]({{ "/assets/img/examples/minerva.gif" | relative_url }} "Minerva Source Model")

### Silhouettes
See here: [/examples/minerva/silhouettes/*.png](https://gitlab.com/mirorasu/visualhull/-/blob/main/examples/minerva/silhouettes)

The silhouette pictures were taken in the Blender project included in the git project folder [/examples/minerva/silhouettes/photo_studio.blend](https://gitlab.com/mirorasu/visualhull/-/blob/main/examples/minerva/silhouettes/photo_studio.blend) in 8 rotations at 45°-steps around the y-axis (in Blender: z-axis).
The positions of the camera were adjusted manually so that no significant floating point errors occur.

### Parameters
See here: [/examples/minerva/parameters_full.json](https://gitlab.com/mirorasu/visualhull/-/blob/main/examples/minerva/parameters_full.json)

The positions and rotations for the camera settings were recorded manually.
Unlike the Blender meter units, I chose to use decimeters (10 dm = 1 m).

The field-of-view (fov) of 40° was preset in the Blender project and added to the parameters.

The cube size was set to 0.5, the smaller the number, the more details can be captured.
Keep in mind that a higher voxel count means longer computing time.

### Result
After running the VisualHull code with the given parameters json-file, the resulting model got created:
![Minerva Result]({{ "/assets/img/examples/minerva_result.gif" | relative_url }} "Minerva Result")