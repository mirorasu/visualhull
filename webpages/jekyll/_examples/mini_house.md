---
title: Mini House
permalink: /examples/mini-house/
description: Mini House Example
---

This example is supposed to represent a small house and originally was created as a test.
The resulting model is contained in the picture on the [front page]({{ site.baseurl }}/).

### Silhouettes
See here: [/examples/mini_house/*.png](https://gitlab.com/mirorasu/visualhull/-/blob/main/examples/mini_house)

<img src="{{ "/assets/img/examples/mini_house_0.png" | relative_url }}" alt="Mini House Silhouette Front" width="64"/>

<img src="{{ "/assets/img/examples/mini_house_1.png" | relative_url }}" alt="Mini House Silhouette Side" width="64"/>

The silhouette pictures were drawn and are actually 8x8 png files.
The first silhouette should show the house from its front side and the second from its side.
The projection mode is orthographic.

### Parameters
The parameters were chosen, such that the resulting model is just a direct representation of the drawn silhouettes.
Therefore the dimensions of the VoxelGrid are 8x8x8.

### Result
After running the VisualHull code with the given parameters json-file, the resulting model got created:
![Mini House Result]({{ "/assets/img/examples/mini_house_result.gif" | relative_url }} "Mini House Result")