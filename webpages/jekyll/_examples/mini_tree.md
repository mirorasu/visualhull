---
title: Mini Tree
permalink: /examples/mini-tree/
description: Mini Tree Example
---

This example is supposed to represent a small tree and originally was created as a test.
The resulting model is contained in the picture on the [front page]({{ site.baseurl }}/).
The procedures were analogous to that of the [Mini House]({{ "/examples/mini-house/" | relative_url }}).

### Silhouettes
See here: [/examples/mini_tree/*.png](https://gitlab.com/mirorasu/visualhull/-/blob/main/examples/mini_tree)

<img src="{{ "/assets/img/examples/mini_tree_0.png" | relative_url }}" alt="Mini Tree Silhouette Front" width="64"/>

<img src="{{ "/assets/img/examples/mini_tree_1.png" | relative_url }}" alt="Mini Tree Silhouette Side" width="64"/>

The silhouette pictures were drawn and are actually 8x12 and 10x12 png files.
The first silhouette should show the tree from its front side and the second from its side.
The projection mode is orthographic.

### Parameters
The parameters were chosen, such that the resulting model is just a direct representation of the drawn silhouettes.
Therefore the dimensions of the VoxelGrid are 8x12x10.

## Procedure
For this example, there is also an animation, which depicts the procedure of creating a visual hull out of silhouettes, starting with a cuboid, consisting of voxels:
![Mini Tree Procedure]({{ "/assets/img/examples/procedure.gif" | relative_url }} "Mini Tree Procedure")

### Result
After running the VisualHull code with the given parameters json-file, the resulting model got created:
![Mini Tree Result]({{ "/assets/img/examples/mini_tree_result.gif" | relative_url }} "Mini Tree Result")