---
title: Sphinx
permalink: /examples/sphinx/
description: Sphinx Example
---

This example is a 3D-Model recreation of an Egyptian Sphinx-Souvenir.

### Source Model
The original object is smaller than 10cm x 10cm x 20cm, depicting the famous Great Sphinx of Giza statue in Egypt.

### Silhouettes
For the silhouettes, I took pictures of the original object. To fetch as many details as possible, the model was rotated at the "origin" in 30° steps, resulting in 12 pictures. Additionally the object was photographed from above, to get more details on the front legs and the overall form. Click here for all pictures: [/examples/sphinx/silhouettes/source/original/*.png](https://gitlab.com/mirorasu/visualhull/-/blob/main/examples/sphinx/source/original/).

![Sphinx Pictures]({{ "/assets/img/examples/sphinx_pictures.gif" | relative_url }} "Sphinx Pictures")

Lastly these pictures were first scaled down to a more useable resolution and then the silhouettes were extracted via [GIMP](https://www.gimp.org/). Note that the silhouettes may contain grey noise, but as long as the *dark_spot* parameter is set to black, these impurities will be ignored.

![Sphinx Silhouettes]({{ "/assets/img/examples/sphinx_silhouettes.gif" | relative_url }} "Sphinx Silhouettes")

### Parameters
See here: [/examples/sphinx/parameters_full.json](https://gitlab.com/mirorasu/visualhull/-/blob/main/examples/sphinx/parameters_full.json)

The positions and rotations for the camera settings were calculated trigonometrically via the rotation angle.
Note that the angle of the object is actually different from the relative camera rotation.
The object was rotated to the left, i.e. a negative rotation along the y-axis.
Therefore the relative camera rotation represent the positive rotation of each given object rotation.

The parameters' length unit is recorded in centimeters

The field-of-view (fov) was set according to the camera lens and its settings.
Therefore we have a different fov for the top perspective.

### Result
There are two output object files:

The file [/examples/sphinx/output.obj](https://gitlab.com/mirorasu/visualhull/-/blob/main/examples/sphinx/output.obj) was created with a voxel size of 0.2 (cm), where as the file [/examples/sphinx/output_double_precision.obj](https://gitlab.com/mirorasu/visualhull/-/blob/main/examples/sphinx/output_double_precision.obj) was created with a voxel size of 0.1 (cm).
The Volume Precision is not the double as the name would suggest, as there are 8 times as many voxels in the canvas voxel grid.

![Sphinx Result]({{ "/assets/img/examples/sphinx_result.gif" | relative_url }} "Sphinx Result")